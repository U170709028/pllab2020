#include <stdio.h>
#include <stdlib.h>
#include <math.h>
FILE *outp;
float arb_func(float x){
    return (x * log10(x))-1.2;


}

float rf(float *x, float x0, float x1, float fx0, float fx1, int *itr){

    *x = ((x0 * fx1)-(x1*  fx0))/ (fx1-fx0);
    ++(*itr);
    printf("iteration no %d= %.5f \n", *itr , *x);
    fprintf(outp, "iteration no %d=%.4f \n", *itr, *x);

}


int main()
{
    int itr=0, maxitr;
    float x0, x1, x_curr, x_next, error;
    outp= fopen("rf.txt", "w");
    printf("enter interval values [x0, x1], allowed error and max iteration:");
    scanf("%f %f %f %d", &x0, &x1, &error, &maxitr);
    rf(&x_curr, x0, x1, arb_func(x0), arb_func(x1), &itr );
    do{
        if(arb_func(x0)*arb_func(x_curr)<0)
            x1=x_curr;
        else
            x0= x_curr;
        rf(&x_next, x0, x1, arb_func(x0), arb_func(x1), &itr);
        if(fabs(x_next - x_curr)< error){
            printf("after %d iterations, root = %6.4f\n", itr, x_next);
            return 0;

        }
        x_curr=x_next;

    }
    while(itr<maxitr);
    printf("Solution does not converge or iterations not sufficient:\n");
    fclose(outp);
    return 1;
}
